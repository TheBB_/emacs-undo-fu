
##########
Change Log
##########

- Version 0.2 (2020-01-12)

  - Linear redo support (which wont undo).
  - Evil-Mode attribute not to repeat these undo/redo actions.
  - Fix counting bug with ``undo-fu-only-redo-all``.
  - Add ``undo-fu-allow-undo-in-region`` option.

- Version 0.1 (2019-12-14)

  Initial release.
